package main

import (
 "blue/trace"
 "github.com/gorilla/websocket"
 "log"
 "net/http"
)

type room struct {
 //	forward is a channel that holds incoming messages that should be forwarded to the other clients
 forward chan []byte
 //join is a channel for clients wishing to join the room
 join chan *client
 //leave is a channel for clients wishing to leave the room
 leave chan *client
 //client holds all current clients in this room.
 clients map[*client]bool  //note, this is a map
 //tracer will receive trace information of activity in the room
 tracer trace.Tracer
}

func newRoom() *room {
 return &room{
  forward: make(chan []byte),
  join: make(chan *client),
  leave: make(chan *client),
  clients: make(map[*client]bool),
 }
}

func (r *room) run(){
 for {
  select {
   case client := <-r.join:
    //joining
    r.clients[client] = true
    case client := <-r.leave:
  //   leaving
    delete(r.clients, client)
    close(client.send)

    case msg := <-r.forward:
    //    forward message to all clients
    for client := range r.clients {
     client.send <- msg
    }
  }
 }
}

const (
 sockerBufferSize = 1024
 messageBufferSize = 256
)

var upgrader = &websocket.Upgrader{ReadBufferSize: sockerBufferSize, WriteBufferSize: sockerBufferSize}

func (r *room) ServeHTTP(w http.ResponseWriter, req *http.Request) {
 socket, err := upgrader.Upgrade(w, req, nil)
 if err != nil {
  log.Fatal("ServeHTTP:", err)
  return
 }
 client := &client{
  socket: socket,
  send: make(chan []byte, messageBufferSize),
  room: r,
 }
 r.join <- client
 defer func() { r.leave <- client }()

 go client.write()
 client.read()
}

